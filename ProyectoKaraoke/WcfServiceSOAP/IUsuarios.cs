﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IUsuario" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IUsuarios
    {
        [OperationContract]
        Usuario CreaUsuario(Usuario usuario);

        [OperationContract]
        List<Usuario> Listar();

        [OperationContract]
        bool Login(Usuario usuario);

        [OperationContract]
        Usuario ModificaUsuario(Usuario usuario);

        [OperationContract]
        bool EliminaUsuario(string codigo);

        [OperationContract]
        Usuario Obtener(string codigo);

    }
}
