﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace WcfServices.Persistencia
{
    public class UsuarioDAO
    {
        private string cadenaConexion ="Data Source=(local); Initial Catalog=DSD; integrated security=SSPI; persist security info=False;";

        public Usuario Crear(Usuario usu) {

            
            Usuario usuario = null;
            usuario = Obtener(null);
            int ejec;
            bool crea=false;
            string sql = "insert into Usuario (codigo,nombre,apellido,foto,clave,correo) values ('" + usuario.codigo  + "','" + usu.nombre + "','" + usu.apellido + "','" + usu.foto + "','" + usu.clave + "','" + usu.correo + "')";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion)) {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql,conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@codigo", usu.codigo));
                    //comando.Parameters.Add(new SqlParameter("@nombre", usu.nombre));
                    //comando.Parameters.Add(new SqlParameter("@apellido", usu.apellido));
                    //comando.Parameters.Add(new SqlParameter("@contenido", usu.contenido));
                    //comando.Parameters.Add(new SqlParameter("@foto", usu.foto));
                    //comando.Parameters.Add(new SqlParameter("@clave", usu.clave));
                    //comando.Parameters.Add(new SqlParameter("@correo", usu.correo));
                    ejec= (int)comando.ExecuteNonQuery();
                }
                if (ejec> 0)
                {
                    crea = true;
                }
            }
            //usu = Obtener(usuario.codigo);
            return usuario;
        }

        public Usuario Obtener(string codigo)
        {
            Usuario usu = null;
            string sql="";
            if (codigo!=null){
                sql = "select * from usuario where codigo='" + codigo + "'";
            }else{
                sql = "select LEFT('U',1)+ Right('0000'+ Cast(Cast((Select max(SubString(codigo,2,4)) from usuario) As Int)+1 As VarChar(8)),4) 'codigo'";
            }
            
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@codigo", codigo));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            usu = new Usuario();
                            if (codigo == null)
                            {
                                usu.codigo = (string)resultado["codigo"];                                
                            }
                            else
                            {
                                usu.codigo = (string)resultado["codigo"];
                                usu.nombre = (string)resultado["nombre"];
                                usu.apellido = (string)resultado["apellido"];
                                usu.foto = (string)resultado["foto"];
                                usu.clave = (string)resultado["clave"];
                                usu.correo = (string)resultado["correo"];
                                if (resultado["contenido"].GetType() == typeof(DBNull))
                                {
                                    usu.contenido = "";
                                }
                                else { usu.contenido = (string)resultado["contenido"]; };
                            }
                            
                                
                        }
                    }
                    
                }
            }
            return usu;
        }

        public Usuario Modificar(Usuario usu)
        {
            Usuario usumodificado = null;
            int ejec;
            bool edit = false;
            //string sql = "update usuario set nombre=@nombre,apellido=@apellido where codigo=@codigo";

            string sql = "update usuario set nombre='" + usu.nombre + "',apellido='" + usu.apellido + "',foto='" + usu.foto + "',clave='" + usu.clave + "',correo='" + usu.correo + "' where codigo= '" + usu.codigo + "'";

            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@codigo", usumodificado.codigo));
                    //comando.Parameters.Add(new SqlParameter("@nombre", usumodificado.nombre));
                    //comando.Parameters.Add(new SqlParameter("@apelllido", usumodificado.apellido));
                    ejec = (int)comando.ExecuteNonQuery();
                }
                if (ejec > 0)
                {
                    edit = true;
                }
            }
            usumodificado = Obtener(usu.codigo);
            return usumodificado;
        }

        public bool Eliminar(string codigo)
        {
            //Usuario usuarioeliminar = null;
            bool elim = false;
            int ejecucion;
            string sql = "delete from usuario where codigo='"+ codigo + "' ";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@codigo", codigo));                    
                    ejecucion =(int)comando.ExecuteNonQuery();
                }
                if (ejecucion>0)
                    {
                        elim = true;
                    }
            }
            return elim;
        }

        public List<Usuario> Listar()
        {
            Usuario usu = null;
            List<Usuario> lstusuario = new List<Usuario>();
            string sql = "select * from usuario";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while(resultado.Read())
                        {
                            usu = new Usuario();                           
                                usu.codigo = (string)resultado["codigo"];
                                usu.nombre = (string)resultado["nombre"];
                                usu.apellido = (string)resultado["apellido"];
                                //usu.foto = (string)resultado["foto"];
                                usu.clave = (string)resultado["clave"];
                                usu.correo = (string)resultado["correo"];
                                if(resultado["contenido"].GetType() == typeof(DBNull)){
                                    usu.contenido = "";
                                }
                                else { usu.contenido = (string)resultado["contenido"];};
                            lstusuario.Add(usu);
                        }
                    }

                }
            }
            return lstusuario;
        }


        public bool Login(Usuario usuario)
        {
            //Usuario usu = null;
            int ejec = 0;
            string result;
            bool exec = false;
            string sql = "Select count(*) from usuario where correo='"+ usuario.correo.Trim() + "' and clave='" + usuario.clave.Trim() + "' ";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@correo", usuario.email));
                    //comando.Parameters.Add(new SqlParameter("@clave", usuario.clave));
                    int ejecucion = (int)comando.ExecuteScalar();
                    if (ejecucion>0)
                    {
                        exec = true;
                    }
                }
                return exec;
            }
            

        }



    }
}