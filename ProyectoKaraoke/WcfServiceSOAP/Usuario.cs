﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfServices
{
    [DataContract]
    public class Usuario
    {
        [DataMember]
        public string codigo { get; set; }

        [DataMember]
        public string nombre { get; set; }

        [DataMember]
        public string apellido { get; set; }

        [DataMember]
        public string estado { get; set; }

        [DataMember]
        public string contenido{ get; set; }

        [DataMember]
        public string correo { get; set; }

        [DataMember]
        public string clave { get; set; }

        [DataMember]
        public string foto { get; set; }

    }
}