﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServices.Persistencia;


namespace WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Usuario" en el código, en svc y en el archivo de configuración a la vez.
    public class Usuarios : IUsuarios
    {

        private UsuarioDAO usuarioDAO = new UsuarioDAO();

        public Usuario CreaUsuario(Usuario usuario)
        {
                return usuarioDAO.Crear(usuario);
        }

        public Usuario ModificaUsuario(Usuario usuario)
        {
            
                return usuarioDAO.Modificar(usuario);            
        }

        public bool Login(Usuario usuario)
        {
            
            return usuarioDAO.Login(usuario);            
        }

        public List<Usuario> Listar()
        {

            return usuarioDAO.Listar();
        }

        public bool EliminaUsuario(string codigo)
        {

            return usuarioDAO.Eliminar(codigo);

        }

        public Usuario Obtener(string codigo)
        {
            return usuarioDAO.Obtener(codigo);
        }


    }
}
