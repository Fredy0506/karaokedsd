﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WSCanciones.Dominio;
using WSCanciones.Errores;
using WSCanciones.Persistencia;

namespace WSCanciones
{
    public class Canciones : ICanciones
    {
        private CancionADO cancionDB = new CancionADO();
        public Cancion CrearCancion(Cancion cancionACrear)
        {
            Cancion cancionExistente = cancionDB.Obtener(cancionACrear.Codigo);
            if (cancionExistente != null)
            {
                throw new WebFaultException<Excepciones>(new Excepciones()
                {
                    Codigo = "102", 
                    Description = "Cancion duplicada"
                }, HttpStatusCode.Conflict); 
            }
            return cancionDB.Crear(cancionACrear);
        }

        public void DoWork()
        {
            throw new NotImplementedException();
        }

        public void EliminarCancion(string codigo)
        {
            cancionDB.Eliminar(int.Parse(codigo));
        }

        public List<Cancion> ListarCanciones()
        {
            return cancionDB.Listar();
        }

        public Cancion ModificarCancion(Cancion cancionAModificar)//--Modificar--
        {
            return cancionDB.Modificar(cancionAModificar);
        }

        public Cancion ObtenerCancion(string codigo)
        {
            return cancionDB.Obtener(int.Parse(codigo));
        }
    }
}
