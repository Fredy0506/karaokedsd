﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WSCanciones.Dominio;

namespace WSCanciones.Persistencia
{
    public class CancionADO
    {
        private string conectar = "Data Source=azrdb01.database.windows.net;" +
    "Initial Catalog=DSD;Persist Security Info=True;User ID=admazrdb01;Password=Pa$$w0rd";
        public Cancion Crear(Cancion cancionACrear)
        {
            Cancion nuevaCancion = null;
            string sql = "INSERT INTO t_productos VALUES (@cod, @nombre, @precio, @disponibilidad, @cantidad)";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", cancionACrear.Codigo));
                    comando.Parameters.Add(new SqlParameter("@artista", cancionACrear.Artista));
                    comando.Parameters.Add(new SqlParameter("@nombre", cancionACrear.Nombre));
                    comando.ExecuteNonQuery();
                }
            }
            nuevaCancion = Obtener(cancionACrear.Codigo);
            return cancionACrear;
        }

        public Cancion Obtener(int codigo)
        {
            Cancion cancionEncontrada = null;
            string sql = "SELECT * FROM t_canciones WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            cancionEncontrada = new Cancion()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Artista = (string)resultado["tx_artista"],
                                Nombre = (string)resultado["tx_nombre"]
                            };
                        }
                    }
                }
            }
            return cancionEncontrada;
        }

        public Cancion Modificar(Cancion cancionAModificar)//--Modificar--
        {
            Cancion alumnoModificado = null;
            string sentencia = "UPDATE t_canciones SET tx_artista=@artista, tx_nombre=@nombre WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sentencia, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", cancionAModificar.Codigo));
                    comando.Parameters.Add(new SqlParameter("@artista", cancionAModificar.Artista));
                    comando.Parameters.Add(new SqlParameter("@nombre", cancionAModificar.Nombre));
                    comando.ExecuteNonQuery();
                }
            }
            alumnoModificado = Obtener(cancionAModificar.Codigo);
            return alumnoModificado;
        }

        public void Eliminar(int codigo) {
            string sentencia = "DELETE FROM t_canciones WHERE nu_cod=@cod";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sentencia, conex))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    comando.ExecuteNonQuery();
                }
            }
        }

        public List<Cancion> Listar()
        {
            List<Cancion> cancionesEncontradas = new List<Cancion>();
            Cancion cancionEncontrada = null;
            string sql = "select * from t_canciones";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sql, conex))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            cancionEncontrada = new Cancion()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Artista = (string)resultado["tx_artista"],
                                Nombre = (string)resultado["tx_nombre"]
                            };
                            cancionesEncontradas.Add(cancionEncontrada);
                        }
                    }
                }
            }
            return cancionesEncontradas;
        }
    }
}