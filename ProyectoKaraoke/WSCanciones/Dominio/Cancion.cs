﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WSCanciones.Dominio
{
    public class Cancion
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember(IsRequired = false)]
        public string Artista { get; set; }

        [DataMember(IsRequired = false)]
        public string Nombre { get; set; }
    }
}