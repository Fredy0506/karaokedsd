﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WSCanciones.Dominio;

namespace WSCanciones
{
    [ServiceContract]
    public interface ICanciones
    {
        [OperationContract]//crear
        [WebInvoke(Method = "POST", UriTemplate = "Canciones", ResponseFormat = WebMessageFormat.Json)]
        Cancion CrearCancion(Cancion cancionACrear);

        [OperationContract]//Obtener
        [WebInvoke(Method = "GET", UriTemplate = "Canciones/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Cancion ObtenerCancion(string codigo);

        [OperationContract]//--Modificar--
        [WebInvoke(Method = "PUT", UriTemplate = "Canciones", ResponseFormat = WebMessageFormat.Json)]
        Cancion ModificarCancion(Cancion cancionAModificar);

        [OperationContract]//Eliminar
        [WebInvoke(Method = "DELETE", UriTemplate = "Canciones/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        void EliminarCancion(string codigo);

        [OperationContract]//Listar
        [WebInvoke(Method = "GET", UriTemplate = "Canciones", ResponseFormat = WebMessageFormat.Json)]
        List<Cancion> ListarCanciones();
    }
}
