﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WSPedidos.Errores
{
    public class Excepciones
    {
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public string Description { get; set; }
    }
}