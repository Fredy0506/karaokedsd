﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WSPedidos.Dominio;

namespace WSPedidos
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPedidos" in both code and config file together.
    [ServiceContract]
    public interface IPedidos
    {
        [OperationContract]
        Pedido CrearPedido(Pedido pedidoACrear);

        [OperationContract]
        Pedido ObtenerPedido(int codigo);

        [OperationContract]
        Pedido ModificarPedido(Pedido pedidoAModificar);

        [OperationContract]
        void EliminarPedido(int codigo);

        [OperationContract]
        List<Pedido> ListarPedidos();
    }
}
