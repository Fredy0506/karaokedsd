﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WSPedidos.Dominio;
using WSPedidos.Errores;
using WSPedidos.Persistencia;

namespace WSPedidos
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Pedidos" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Pedidos.svc or Pedidos.svc.cs at the Solution Explorer and start debugging.
    public class Pedidos : IPedidos
    {
        private PedidoADO PedidoDB = new PedidoADO();

        public Pedido CrearPedido(Pedido pedidoACrear)
        {
            if (PedidoDB.Obtener(pedidoACrear.Codigo) != null) //Ya existe
            {
                throw new FaultException<Excepciones>(
                    new Excepciones()
                    {
                        Codigo = "101",
                        Description = "El pedido ya existe",
                    },
                    new FaultReason("Error al intentar creación"));
            }
            return PedidoDB.Crear(pedidoACrear);
        }

        public void EliminarPedido(int codigo)
        {
            PedidoDB.Eliminar(codigo);
        }

        public List<Pedido> ListarPedidos()
        {
            return PedidoDB.Listar();
        }

        public Pedido ModificarPedido(Pedido pedidoAModificar)
        {
            return PedidoDB.Modificar(pedidoAModificar);
        }

        public Pedido ObtenerPedido(int codigo)
        {
            return PedidoDB.Obtener(codigo);
        }
    }
}
