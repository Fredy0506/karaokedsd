﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WSPedidos.Dominio
{
    public class Pedido
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public int Usuario { get; set; }

        [DataMember(IsRequired = false)]
        public string Fecha { get; set; }

        [DataMember(IsRequired = false)]
        public string Hora { get; set; }
    }
}