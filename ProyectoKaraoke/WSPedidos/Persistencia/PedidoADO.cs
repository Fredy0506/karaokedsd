﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WSPedidos.Dominio;
using System.Data.SqlClient;


namespace WSPedidos.Persistencia
{
    public class PedidoADO
    {
        private string conectar = "Data Source=azrdb01.database.windows.net;" +
            "Initial Catalog=DSD;Persist Security Info=True;User ID=admazrdb01;Password=Pa$$w0rd";
        public Pedido Crear(Pedido pedidoACrear)
        {
            Pedido nuevoPedido = null;
            string sql = "INSERT INTO t_pedidos VALUES (@cod, @usuario, @fecha, @hora)";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", pedidoACrear.Codigo));
                    comando.Parameters.Add(new SqlParameter("@usuario", pedidoACrear.Usuario));
                    comando.Parameters.Add(new SqlParameter("@fecha", pedidoACrear.Fecha));
                    comando.Parameters.Add(new SqlParameter("@hora", pedidoACrear.Hora));
                    comando.ExecuteNonQuery();
                }
            }
            nuevoPedido = Obtener(pedidoACrear.Codigo);
            return pedidoACrear;
        }

        public Pedido Obtener(int codigo)
        {
            Pedido pedidoEncontrado = null;
            string sql = "SELECT * FROM t_pedidos WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            pedidoEncontrado = new Pedido()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Usuario = (int)resultado["nu_user_cod"],
                                Fecha = (string)resultado["tx_fecha"],
                                Hora = (string)resultado["tx_hora"]
                            };
                        }
                    }
                }
            }
            return pedidoEncontrado;
        }

        public Pedido Modificar(Pedido pedidoAModificar)
        {
            Pedido pedidoModificado = null;
            string sql = "UPDATE t_pedidos SET " +
                "nu_user_cod=@usuario, tx_fecha=@fecha, tx_hora=@hora WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", pedidoAModificar.Codigo));
                    comando.Parameters.Add(new SqlParameter("@usuario", pedidoAModificar.Usuario));
                    comando.Parameters.Add(new SqlParameter("@fecha", pedidoAModificar.Fecha));
                    comando.Parameters.Add(new SqlParameter("@hora", pedidoAModificar.Hora));
                    comando.ExecuteNonQuery();
                }
            }
            pedidoModificado = Obtener(pedidoAModificar.Codigo);
            return pedidoModificado;
        }

        public void Eliminar(int codigo)
        {
            string sql = "DELETE FROM t_pedidos WHERE nu_cod=@cod";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sql, conex))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    comando.ExecuteNonQuery();
                }
            }
        }

        public List<Pedido> Listar()
        {
            List<Pedido> pedidosEncontrados = new List<Pedido>();
            Pedido pedidoEncontrado = null;
            string sql = "select * from t_pedidos";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sql, conex))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            pedidoEncontrado = new Pedido()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Usuario = (int)resultado["nu_user_cod"],
                                Fecha = (string)resultado["tx_fecha"],
                                Hora = (string)resultado["tx_hora"]
                            };
                            pedidosEncontrados.Add(pedidoEncontrado);
                        }
                    }
                }
            }
            return pedidosEncontrados;
        }
    }
}