﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IComprobante" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IComprobante
    {
        [OperationContract]
        bool Registro(Comprobantes obj);

        [OperationContract]
        List<Comprobantes> Listar();
    }
}
