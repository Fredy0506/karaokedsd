﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfServices
{
    [DataContract]
    public class Comprobantes
    {
        [DataMember]
        public string Codigo { get; set; }

        [DataMember]
        public string CodigoComprobante { get; set; }

        [DataMember]
        public string NumeroFactura { get; set; }

        [DataMember]
        public string CodigoCliente { get; set; }

        [DataMember]
        public string CodigoReserva { get; set; }

        [DataMember]
        public string Direccion { get; set; }

        [DataMember]
        public string Telefono { get; set; }

        [DataMember]
        public DateTime Fecha { get; set; }

        [DataMember]
        public decimal IGV { get; set; }

        [DataMember]
        public decimal SubTotal { get; set; }

        [DataMember]
        public decimal Total { get; set; }

    }
}