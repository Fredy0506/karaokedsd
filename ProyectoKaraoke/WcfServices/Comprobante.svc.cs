﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WcfServices.Persistencia;

namespace WcfServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Comprobante" en el código, en svc y en el archivo de configuración a la vez.
    public class Comprobante : IComprobante
    {
        #region Miembros de IComprobante

        private ComprobanteDAO ComprobanteDAO = new ComprobanteDAO();

        public bool Registro(Comprobantes obj)
        {
            return ComprobanteDAO.Crear(obj);

        }

        public List<Comprobantes> Listar()
        {
            return ComprobanteDAO.Listar();
        }

        #endregion
    }
}
