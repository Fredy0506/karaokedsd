﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WcfServices
{
    [DataContract]
    public class ComprobanteDetalle
    {
        [DataMember]
        public string CodigoComprobante { get; set; }

        [DataMember]
        public string CodigoProducto { get; set; }

        [DataMember]
        public float Cantidad { get; set; }

        [DataMember]
        public float Precio { get; set; }

        [DataMember]
        public float Importe { get; set; }

    }
}