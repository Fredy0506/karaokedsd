﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;

namespace WcfServices.Persistencia
{
    public class ComprobanteDAO
    {
        private string cadenaConexion = "Data Source=(local); Initial Catalog=DSD; integrated security=SSPI; persist security info=False;";

        public bool Crear(Comprobantes com)
        {
            int ejec;
            bool crea = false;
            string sql = "insert into Comprobante (Codigo,CodigoComprobante,NumeroFactura,CodigoCliente,CodigoReserva,Direccion,Telefono,Fecha,IGV,SubTotal,Total) values ('" + com.Codigo + "','" +
                                com.CodigoComprobante + "','" + com.NumeroFactura + "','" + com.CodigoCliente + "','" + com.CodigoReserva + "','" +
                                com.Direccion + "','" + com.Telefono + "','" + com.Fecha  + "','" + com.IGV + "','" +
                                com.SubTotal + "','" + com.Total + "')";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    //comando.Parameters.Add(new SqlParameter("@codigo", usu.codigo));
                    //comando.Parameters.Add(new SqlParameter("@nombre", usu.nombre));
                    //comando.Parameters.Add(new SqlParameter("@apellido", usu.apellido));
                    //comando.Parameters.Add(new SqlParameter("@contenido", usu.contenido));
                    //comando.Parameters.Add(new SqlParameter("@foto", usu.foto));
                    //comando.Parameters.Add(new SqlParameter("@clave", usu.clave));
                    //comando.Parameters.Add(new SqlParameter("@correo", usu.correo));
                    ejec = (int)comando.ExecuteNonQuery();
                }
                if (ejec > 0)
                {
                    crea = true;
                }
            }
            return crea;
        }

        public List<Comprobantes> Listar()
        {
            Comprobantes com= null;
            List<Comprobantes> lstComprobante = new List<Comprobantes>();
            string sql = "select * from Comprobante";
            using (SqlConnection conexion = new SqlConnection(cadenaConexion))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            com = new Comprobantes();
                            com.Codigo = (string)resultado["Codigo"];
                            com.CodigoComprobante = (string)resultado["CodigoComprobante"];
                            com.NumeroFactura = (string)resultado["NumeroFactura"];
                            com.CodigoCliente = (string)resultado["CodigoCliente"];
                            com.CodigoReserva = (string)resultado["CodigoReserva"];
                            com.Direccion = (string)resultado["Direccion"];
                            com.Telefono = (string)resultado["Telefono"];
                            com.Fecha = (DateTime)resultado["Fecha"];
                            com.IGV = (decimal)resultado["IGV"];
                            com.SubTotal = (decimal)resultado["SubTotal"];
                            com.Total = (decimal)resultado["Total"];
                            lstComprobante.Add(com);
                        }
                    }

                }
            }
            return lstComprobante;
        }


    }
}