﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class RegistroProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            registro();
        }


        public void registro()
        {
            try
            {
                ServiceProductos.ProductosClient proxy = new ServiceProductos.ProductosClient();
                ServiceProductos.Producto productoNuevo = new ServiceProductos.Producto();
                ServiceProductos.Producto productoCreado = null;

                //salaNuevo.Id_sala = 22;
                productoNuevo.Codigo = Convert.ToInt32(txtCodigo.Text);
                productoNuevo.Nombre = txtNombre.Text;
                productoNuevo.Cantidad = Convert.ToInt32(txtcantidad.Text);
                productoNuevo.Disponibilidad = txtDisponibilidad.Text;
                productoNuevo.Precio = Convert.ToDecimal(txtPrecio.Text);
                productoCreado = proxy.CrearProducto(productoNuevo);

                if (productoCreado != null)
                {
                    string script = @"<script type='text/javascript'>alert('Se Registró Correctamente el producto " + productoNuevo.Codigo + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }
                btnRegistro.Visible = false;
            }
            catch (Exception ex)
            {

                lblMessage.Text = "ERROR: " + ex;
            }

        }

    }
}