﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {

            UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
            //List<UsuarioService.Usuario> lista = proxy.Listar().ToList<UsuarioService.Usuario>();
            UsuarioService.Usuario usu = new UsuarioService.Usuario();
            bool retorna=false;
            usu.correo = txtUsuario.Text.Trim();
            usu.clave=txtclave.Text.Trim();
            retorna= proxy.Login(usu);
            if (retorna==true)
            {
            Response.Redirect("usuario.aspx");
            }
            

        }
    }
}