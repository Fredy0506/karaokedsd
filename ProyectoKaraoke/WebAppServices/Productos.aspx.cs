﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class Productos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            listarProductos();
        }


        private void listarProductos()
        {
            try
            {
                ServiceProductos.ProductosClient proxy = new ServiceProductos.ProductosClient();
                List<ServiceProductos.Producto> lista = proxy.ListarProductos().ToList<ServiceProductos.Producto>();

                dgProductos.DataSource = lista;
                dgProductos.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        protected void dgProductos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgProductos.PageIndex = e.NewPageIndex;
            listarProductos(); 
        }

        protected void dgProductos_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

    }
}