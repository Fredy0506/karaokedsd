﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class Reservas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            listarReservas();
        }

        private void listarReservas()
        {
            try
            {
                ReservasServices.ReservaServicesClient proxy = new ReservasServices.ReservaServicesClient();
                List<ReservasServices.Reserva> lista = proxy.ListarReservas().ToList<ReservasServices.Reserva>();

                dgReservas.DataSource = lista;
                dgReservas.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        protected void dgReservas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgReservas.PageIndex = e.NewPageIndex;
            listarReservas(); 
        }

        protected void dgReservas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gvrow = dgReservas.Rows[index];
            int codigo = Convert.ToInt16(gvrow.Cells[2].Text);
            ReservasServices.Reserva reservaCancelar;

            ReservasServices.ReservaServicesClient proxy = new ReservasServices.ReservaServicesClient();

            if (e.CommandName.Equals("Eliminar"))
            {
                reservaCancelar = proxy.CancelarReserva(codigo);
                if (reservaCancelar!=null)
                {
                    listarReservas();
                    string script = @"<script type='text/javascript'>alert('Se cancelo la Reserva: " + codigo + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }
            } 


        }
    }
}