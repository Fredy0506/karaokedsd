﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace WebAppServices
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UsuarioServiceReference.UsuariosClient proxy = new UsuarioServiceReference.UsuariosClient();
            List<UsuarioServiceReference.Usuario> lista = proxy.Listar().ToList<UsuarioServiceReference.Usuario>();

            lblcontenido.Text = lista[0].contenido;
        }
    }
}