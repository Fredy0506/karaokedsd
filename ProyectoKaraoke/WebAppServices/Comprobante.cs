﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppServices
{
    public class Comprobante
    {
        public string Codigo { get; set; }

        public string NumeroFactura { get; set; }

        public string Direccion { get; set; }

        public string Telefono { get; set; }

        public string Fecha { get; set; }

        public decimal IGV { get; set; }

        public decimal SubTotal { get; set; }

        public decimal Total { get; set; }

    }
}