﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppServices
{
    public class Local
    {
        public string Administrador { get; set; }
        public string id_local { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Mail { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }

    }
}