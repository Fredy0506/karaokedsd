﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="usuario.aspx.cs" Inherits="WebAppServices.usuario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Blog Personal</title>

	<link rel="shortcut icon" href="images/gt_favicon.png">
	
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/cssGrilla.css">
    
    <link href="css/grid.css" rel="stylesheet" />

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.aspx"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="index.aspx">Inicio</a></li>
					<li><a href="about.aspx">Nosotros</a></li>
					<li><a href="contact.aspx">Contáctanos</a></li>
					<li class="active"><a class="btn" href="login.aspx">SIGN IN / SIGN UP</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	<div class="container">
    <div class="col-md-1 widget">
            <div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="pedidos.aspx">Pedidos</a></li>
					<li><a href="Locales.aspx">Locales</a></li>
                    <li><a href="Salas.aspx">Salas</a></li>
					<li><a href="reservas.aspx">Reservas</a></li>
                    <li><a href="productos.aspx">Productos</a></li>
				</ul>
			</div>
    </div>
    <div class="col-md-2 widget">
    	<div class="row">
			
			<!-- Article main content -->
					
						
<form id="Form1" runat="server"> 
<table border="0" CellPadding="2" cellspacing="0" style="width: 100%">
    <tr>
      <td style="width: 10px;">
      </td>
      <td>
        <center>
        <div style="color: #034af3; font-size:large;">
        <p>Lista de Usuarios</p>
        </div>
       </center>
      </td>
    </tr>
    <tr>
      <td style="width: 10px;">
      </td>
      <td>
        <table border="0" CellPadding="2" cellspacing="0" style="width: 100%">
          <tr>  
            <td align="center">              
            </td>
          </tr>
          <tr>  
            <td>
              &nbsp;
            </td>
          </tr>
          <tr>
            <td align="center">
           
                   <div id="divGridView">   
                    <asp:gridview id="GridView1" runat="server" 
                    width="60%" BorderWidth="1px"
                    cellpadding="4"
                    ForeColor="Black"
                    HeaderStyle-ForeColor="Black"                    
                    AllowPaging="True" 
                    gridlines="Horizontal" 
                    autogeneratecolumns="False"
                    EmptyDataText="No se encontraron registros"
                    CssClass="table table-striped table-bordered table-hover"                                        
                    onrowcommand="GridView1_RowCommand">  
                    <HeaderStyle CssClass="GridCabeceraFijo" />
                    <RowStyle CssClass="GridDetalle" />
                    <AlternatingRowStyle CssClass="GridDetalleAlterno" />
                    <EditRowStyle BackColor="#2461BF" />
            <Columns>                
                 <%-- <asp:TemplateField HeaderText="Editar">
                    <ItemTemplate>                      
                               <asp:imagebutton ID="lnkModificar" 
                               ImageUrl="~/images/Editar.gif" Height="30"
                            CommandName="Update" CausesValidation="false"                            
                            runat="server">
                          </asp:imagebutton>                       
                    </ItemTemplate>                      
                </asp:TemplateField>--%>
                
                <asp:ButtonField CommandName="Modificar" 
                ControlStyle-CssClass="btn btn-info"
                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                    <ControlStyle CssClass="btn btn-info"></ControlStyle>
                </asp:ButtonField>
                <asp:ButtonField CommandName="Eliminar" 
                ControlStyle-CssClass="btn btn-info"
                ButtonType="Button" Text="Delete"
                HeaderText="Delete Record">
                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                </asp:ButtonField>

                <asp:BoundField HeaderText="codigo" DataField="codigo" >                    
                </asp:BoundField>
                <asp:BoundField HeaderText="nombre" DataField="nombre" >                    
                </asp:BoundField>
                <asp:BoundField HeaderText="apellido" DataField="apellido" >                    
                </asp:BoundField>
                <asp:BoundField HeaderText="correo" DataField="correo" >                    
                </asp:BoundField>
               <%-- <asp:TemplateField HeaderText="Eliminar">
                    <ItemTemplate>                      
                               <asp:imagebutton ID="lnkEliminar" ImageUrl="~/images/eliminar.gif" 
                            CommandName="Delete" CausesValidation="false"  Height="30"
                            OnClientClick="return confirm('Estas Seguro de Eliminar?')" 
                            runat="server">
                          </asp:imagebutton>
                       
                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>            
        </asp:gridview>
           </div>
                
                  
              
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>
      </td>
    </tr>
  </table>
  
          <asp:Literal runat="server" ID="litPaging" EnableViewState="False" />
    <p>
        <asp:label id="lblMessage" runat="server" enableviewstate="false" />
    </p>
    </form>
    
    
    </div>
    </div>
	</div>	<!-- /container -->

	

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p><br>
								
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow me</h3>
						<div class="widget-body">
							<p class="follow-me-icons clearfix">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Sobre Nosotros</h3>
						<div class="widget-body">
							<a href="paginas/terms-of-use.html">Términos y condiciones</a><br>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="about.html">About</a> |
								<a href="sidebar-right.html">Sidebar</a> |
								<a href="contact.html">Contact</a> |
								<b><a href="signup.html">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; 2014, Your name. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a> 
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>
</body>
</html>

