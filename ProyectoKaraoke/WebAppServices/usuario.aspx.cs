﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class usuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                listarUsuarios();
            }            
        }

        private void listarUsuarios()
        {
            try
            {
                UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
                List<UsuarioService.Usuario> lista = proxy.Listar().ToList<UsuarioService.Usuario>();

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }
        
        }
        //protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        //{
        //    try
        //    {
        //        bool elim = false;
        //        UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
        //        List<UsuarioService.Usuario> lista = proxy.Listar().ToList<UsuarioService.Usuario>();                
        //        string codigo=GridView1.Rows[e.RowIndex].Cells[1].Text;
        //        elim=proxy.EliminaUsuario(codigo);
        //        if (elim)
        //        {
        //            string script = @"<script type='text/javascript'>alert('Se eliminó Correctamente el registro');</script>";
        //            ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
        //        }
        //        listarUsuarios();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = "ERROR: " + ex;
        //    }

        //}

        //protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        //{
        //    try
        //    {
                
        //        string codigo = GridView1.Rows[e.RowIndex].Cells[1].Text;
        //        Response.Redirect("EditUsuario.aspx?codigo=" + codigo );
        //    }
        //    catch (Exception ex)
        //    {
        //        lblMessage.Text = "ERROR: " + ex;
        //    }

        //}

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gvrow = GridView1.Rows[index];
            string codigo = gvrow.Cells[2].Text;

            if (e.CommandName.Equals("Modificar"))
            {   
                Response.Redirect("EditUsuario.aspx?codigo=" + codigo);
            }
            else if (e.CommandName.Equals("Eliminar")) {
                bool elim = false;                
                UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
                List<UsuarioService.Usuario> lista = proxy.Listar().ToList<UsuarioService.Usuario>();                
                elim = proxy.EliminaUsuario(codigo);
                if (elim)
                {
                    string script = @"<script type='text/javascript'>alert('Se eliminó Correctamente el registro');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }
                listarUsuarios();
            }



        }
    }
}