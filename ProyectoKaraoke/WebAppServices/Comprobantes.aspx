﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comprobantes.aspx.cs" Inherits="WebAppServices.Comprobantes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/cssGrilla.css">    
    <link href="css/grid.css" rel="stylesheet" />

    <title></title>
<script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
</head> 
<body>
    <form id="form1" runat="server">
    <asp:GridView ID="dgComprobante" runat="server"
                    width="60%" 
                    BorderWidth="1px"
                    cellpadding="4"
                    ForeColor="Black"
                    HeaderStyle-ForeColor="Black"                    
                    AllowPaging="True" 
                    gridlines="Horizontal" 
                    autogeneratecolumns="False"
                    EmptyDataText="No se encontraron registros"
                    CssClass="table table-striped table-bordered table-hover">  
                    <HeaderStyle CssClass="GridCabeceraFijo" />
                    <RowStyle CssClass="GridDetalle" />
                    <AlternatingRowStyle CssClass="GridDetalleAlterno" />
                    <EditRowStyle BackColor="#2461BF" />
    <Columns>
    <asp:BoundField HeaderText="Codigo" DataField="Codigo" >                    
    </asp:BoundField>
    <asp:BoundField HeaderText="NumeroFactura" DataField="NumeroFactura" >                    
     </asp:BoundField>
     </Columns>
    </asp:GridView>
    </form>
</body>
</html>
