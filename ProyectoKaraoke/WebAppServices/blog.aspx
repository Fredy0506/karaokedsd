﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="blog.aspx.cs" Inherits="WebAppServices.blog" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="content-language" content="es" />
	<title>Fredy Fernandez Salazar - Página principal</title>
	<meta name="author" content="Fredy Fernandez" />
	<link rel="stylesheet" type="text/css" href="css/screen.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/print.css" media="print" />	
</head>
<body>

<div id="header">
	<div id="logo">
		<h1>Fredy Fernandez</h1>
		<h2>Mi Perfil Profesional</h2>
	</div>
	
	<div id="printheader">
		<p id="smallcopy">Copyright &copy; 2002-2006 Javier Smaldone<br />bajo una licencia de <em>Creative Commons</em>.<br />http://creativecommons.org/licenses/by-nc-sa/2.5/ar/deed.es_AR</p>
	</div>
	<br class="clear" />
</div>

<!--TAB MENU-->
<div id="topcontainer">
	<div id="topnav">
		<h2 class="extra">Secciones</h2>
			<ul id="tabmenu">
		<li><a href="#" class="active">Principal</a></li>
		<li><a href="#">Profesional</a></li>
		<li><a href="#">Fotos</a></li>
		<li><a href="#">Documentos</a></li>
		<li><a href="Login.aspx">Login</a></li>
	</ul>

	</div>
</div>
<!--CENTRAL CONTAINER-->
<div id="central">

	<!--LEFT MENU-->
	<div id="leftmenu">
	 	<h2 class="extra">Sub-secciones</h2>
		<ul id="leftnavlist">
			<li><a href="#" id="leftcurrent"><span class="init">Principal</span></a></li>
	<li><a href="#">Novedades</a></li>

		</ul>
	</div>

	<!--MAIN CONTENT-->
	<div id="content">                    
        <asp:Label ID="lblcontenido" runat="server" Text="Label"></asp:Label>
		<br class="clear"/>
	</div>
</div>

<!--FOOTER-->
<div id="footer">	
	<div id="copyright">
		<p>Copyright &copy; 2016-Fredy Fernandez Salazar <strong> El contenido de este sitio está bajo una licencia<strong></p>
	</div>
</div>

</body>

</html>