﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Globalization;
using System.Collections;

namespace WebAppServices
{
    public partial class RegistrarReserva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                ListarLocales();
                listarSalas();
                listarHoras();

                ddlSala.Enabled = false;

                if (Session["SalasDisponibles"] != null) {
                    ddlCantidadHoras.SelectedValue = Session["CantidadHoras"].ToString();
                    txtHoraInicio.Value = Session["FechaInicio"].ToString();
                    ddlLocal.SelectedValue = Session["local"].ToString();
                    ddlHoras.SelectedValue = Session["HoraReserva"].ToString();
                    ddlSala.SelectedValue = Session["sala"].ToString();
                    btnRegistro.Text = "Registrar";
                    ddlCantidadHoras.Enabled = false;
                    ddlSala.Enabled = false;
                    txtHoraInicio.Disabled = true;
                    ddlCantidadHoras.Enabled = false;
                    ddlHoras.Enabled = false;
                }
                
            }            

        }
        public void ListarLocales()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                "http://190.12.72.40/wslocales/LocalServices.svc/local");
            request.Method = "GET";
            HttpWebResponse response = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            List<Local> listaLocales = js.Deserialize<List<Local>>(tramaJson);
            ddlLocal.DataSource = listaLocales.ToList();
            ddlLocal.DataValueField = "id_local";
            ddlLocal.DataTextField = "Nombre";
            ddlLocal.DataBind();

        }

        private void listarSalas()
        {
            
            try
            {
                SalasServices.SalaServicesClient proxy = new SalasServices.SalaServicesClient();
                List<SalasServices.Sala> listaSalas = proxy.ListarSalas().ToList<SalasServices.Sala>();
                
                ddlSala.DataSource = listaSalas.ToList();
                ddlSala.DataValueField = "Id_sala";
                ddlSala.DataTextField = "Descripcion";
                ddlSala.DataBind();
                ListItem item = new ListItem();
                item.Text = "--Seleccionar--";
                item.Value = "00";
                ddlSala.Items.Add(item);
                ddlSala.Items.FindByValue("00");
                ddlSala.SelectedValue = "00";
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        private void listarHoras()
        {
            try
            {
                ListItem lst;
                for (int i = 1; i < 24; i++)
                {
                    lst= new ListItem();
                    lst.Text = Convert.ToString(i) + ":00";
                    lst.Value= Convert.ToString(i);
                    ddlHoras.Items.Add(lst);
                }
                string Hora = DateTime.Now.AddHours(1).ToShortTimeString();
                Hora = Hora.Substring(0, 2);                
                ddlHoras.DataValueField = "Value_Hora";
                ddlHoras.DataTextField = "Text_Hora";
                ddlHoras.DataBind();
                ddlHoras.Items.FindByValue(Hora);
                ddlHoras.SelectedValue = Hora;
                
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        


        private void listarSalas_Local(int IdLocal)
        {
            try
            {
                SalasServices.SalaServicesClient proxy = new SalasServices.SalaServicesClient();
                List<SalasServices.Sala> listaSalas = proxy.ListarSalasxLocal(IdLocal).ToList<SalasServices.Sala>();

                ddlSala.DataSource = listaSalas.ToList();
                ddlSala.DataValueField = "Id_sala";
                ddlSala.DataTextField = "Descripcion";
                ddlSala.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            if (Session["SalasDisponibles"] != null)
            {
                Registro();                
            }
            else
            {
                Session["CantidadHoras"] = ddlCantidadHoras.SelectedValue;
                Session["FechaInicio"] = txtHoraInicio.Value.Trim();
                Session["HoraReserva"] = ddlHoras.SelectedValue;
                Session["local"] = ddlLocal.SelectedValue;
                Session["Reserva"] = "0";
                Response.Redirect("Salas.aspx");
            }
        }

        public void EliminarSessiones() {
            Session["CantidadHoras"] = null;
            Session["FechaInicio"] = null;
            Session["HoraReserva"] = null;
            Session["local"] = null;
            Session["Reserva"] = null;
            Session["SalasDisponibles"] = null;

        }
        public void Registro() {

            try
            {
                ReservasServices.ReservaServicesClient proxy = new ReservasServices.ReservaServicesClient();
                ReservasServices.Reserva reservaCrear = new ReservasServices.Reserva();
                ReservasServices.Reserva reservaCreado = null;

                reservaCrear.Id_user = 1;
                reservaCrear.Id_local = Convert.ToInt16(ddlLocal.SelectedValue);
                reservaCrear.Id_sala = Convert.ToInt16(ddlSala.SelectedValue);
                //reservaCrear.Fecha_hora_inicio = Convert.ToDateTime(txtHoraInicio.Text);
                reservaCrear.Fecha_hora_inicio = Convertir_fecha(txtHoraInicio.Value.Trim());
                reservaCrear.Horas_reservadas = Convert.ToInt16(ddlCantidadHoras.SelectedValue);
                reservaCrear.Fecha_hora_fin = Convertir_fecha(txtHoraInicio.Value).AddHours(Convert.ToInt16(ddlCantidadHoras.SelectedValue));
                reservaCrear.Descripcion = txtDescripcion.Text;
                reservaCrear.Estado = "RESERVADO";
                Response.Redirect("Reservas.aspx");
                reservaCreado = proxy.CrearReserva(reservaCrear);
                if (reservaCreado != null)
                {
                    string script = @"<script type='text/javascript'>alert('Se Registró Correctamente la Reserva " + reservaCreado.Id_reserva + "');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }
                btnRegistro.Visible = false;
                EliminarSessiones();
            }
            catch (Exception ex)
            {

                lblMessage.Text = "ERROR: " + ex;
            }
        }

        public DateTime Convertir_fecha(string fecha)
        {
            CultureInfo ci = new CultureInfo("en-US");
            string Hora = DateTime.Now.ToShortTimeString();
            //string anio = fecha.Substring(6, 4);  //"04/28/2017"
            //string mes = fecha.Substring(0,2);
            //string dia = fecha.Substring(3, 2);
            //DateTime fechafin = DateTime.Parse(dia + "/" + mes + "/" + anio).ToShortDateString();

            //DateTime fechafin = DateTime.Parse(fecha + " " + Hora);
            string fechaf = DateTime.Now.ToShortTimeString();
            DateTime fechafin = Convert.ToDateTime(fechaf);
            return fechafin;
        }

        protected void ddlLocal_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int IdLocal = Convert.ToInt16(ddlLocal.Items[ddlLocal.SelectedIndex].Value);
            //int IdLocal = ddlLocal.Items.IndexOf(ddlLocal.Items.FindByText("id_local"));
            int IdLocal = Convert.ToInt16(ddlLocal.SelectedItem.Value);
            listarSalas_Local(IdLocal);




        }
    }
}