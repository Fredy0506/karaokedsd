﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistrarReserva.aspx.cs" Inherits="WebAppServices.RegistrarReserva" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>   

    
<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" media="screen"
     href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css"/>
		<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Registro de Reserva</title>

	<link rel="shortcut icon" href="images/gt_favicon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>




	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.aspx"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="index.aspx">Inicio</a></li>
					<li><a href="about.aspx">Nosotros</a></li>
					<li><a href="contact.aspx">Contáctanos</a></li>
					<li class="active"><a class="btn" href="login.aspx">SIGN IN / SIGN UP</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	
	<!-- container -->
	<div class="container">   
    
   
    

		<div class="row">			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
            								
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">Registre una Nueva Reserva</h3>							
							<hr>


							<form runat="server" id="form1">
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
							
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">	
                            <ContentTemplate>
								<div class="top-margin">
									<label>Local: </label>
                                    <br/>
                                    
                                    
									<asp:DropDownList ID="ddlLocal" runat="server" class="form-control"  
                                            AutoPostBack="true" onselectedindexchanged="ddlLocal_SelectedIndexChanged"></asp:DropDownList>
                                    
								</div>
                                <div class="top-margin">
									<label>Sala: </label>
                                    <br/>
									<asp:DropDownList ID="ddlSala" runat="server" class="form-control"></asp:DropDownList>
								</div>
                                </ContentTemplate>
                                <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlLocal" EventName="SelectedIndexChanged" />
                                </Triggers>
                                
                                    </asp:UpdatePanel>
                                <div class="top-margin">
									<label>Fecha Hora Inicio:</label>
                                    <br/>
                                    <div>
                                     <div id="datetimepicker" class="input-append date">
                                              <input type="text" runat="server" id="txtHoraInicio"></input>
                                              <span class="add-on">
                                                <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                              </span>
                                            </div>
                <script type="text/javascript"
                 src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js">
                </script> 
                <script type="text/javascript"
                src="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/js/bootstrap.min.js">
                </script>
                <script type="text/javascript"
                src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
                </script>
                <script type="text/javascript" src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
                </script>            

         <script type="text/javascript">
             //$.datetimepicker.setDefaults($.datetimepicker.regional["es"]);
             $('#datetimepicker').datetimepicker({
                 //format: 'dd/MM/yyyy hh:mm:ss',
                 format: 'dd/MM/yyyy',
                 //language: 'pt-BR',
                 firstDay: 1
                 //}).datetimepicker("setDate", new Date());
             });
    </script>
                                <div class="top-margin">
									<label>Hora Reserva:</label>
                                    <asp:DropDownList ID="ddlHoras" runat="server" class="form-control"  
                                            AutoPostBack="true" DataTextField="Text_Hora" 
                                        DataValueField="Value_Hora">
                                    </asp:DropDownList>
								</div>
                                <div class="top-margin">
									<label>Cantidad de Horas:</label>
                                    <asp:DropDownList ID="ddlCantidadHoras" runat="server" class="form-control"  
                                            DataTextField="Text_reserva" 
                                        DataValueField="Value_reserva">
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                    </asp:DropDownList>
								</div>                           
                                <div class="top-margin">
									<label>Descripcion:</label>
									<asp:TextBox ID="txtDescripcion" runat="server" class="form-control" ></asp:TextBox>
								</div>                                
								<hr>
								<div class="row">									
									<div class="col-lg-4 text-right">
                                <asp:Button ID="btnRegistro" runat="server" class="btn btn-action" Text="Ver Disponilidad" 
                                            onclick="btnRegistro_Click"></asp:Button>
                                            <asp:label id="lblMessage" runat="server" enableviewstate="false" />
									</div>
								</div>
                                
							</form>
						</div>
					</div>
				</div>
			</article>
            <!-- /Article -->
            <div id="divlistado" runat="server"> 
            <center><b><a href="Reservas.aspx">Listado de Reservas</a></b></center> 
        </div>
	    </div>
     </div><!-- /container -->


	

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p><br>
								
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow me</h3>
						<div class="widget-body">
							<p class="follow-me-icons clearfix">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Sobre Nosotros</h3>
						<div class="widget-body">
							<a href="paginas/terms-of-use.html">Términos y condiciones</a><br>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="about.html">About</a> |
								<a href="sidebar-right.html">Sidebar</a> |
								<a href="contact.html">Contact</a> |
								<b><a href="signup.html">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; 2014, Your name. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a> 
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>
</body>
</html>


