﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

namespace WebAppServices
{
    public partial class blog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
            List<UsuarioService.Usuario> lista = proxy.Listar().ToList<UsuarioService.Usuario>();

            lblcontenido.Text = lista[0].contenido;
        }
    }
}