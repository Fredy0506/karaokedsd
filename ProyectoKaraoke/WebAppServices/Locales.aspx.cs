﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace WebAppServices
{
    public partial class Locales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ListarLocales();
        }

        public void ListarLocales()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                "http://190.12.72.40/wslocales/LocalServices.svc/local");
            request.Method = "GET";
            HttpWebResponse response = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            List<Local> listaLocales = js.Deserialize<List<Local>>(tramaJson);
            dgLocales.DataSource = listaLocales.ToList();
            dgLocales.DataBind();
        }

        protected void dgLocales_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void dgLocales_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgLocales.PageIndex = e.NewPageIndex;
            ListarLocales(); 
        }
    }
}