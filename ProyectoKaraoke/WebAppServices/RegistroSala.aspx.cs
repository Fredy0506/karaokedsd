﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace WebAppServices
{
    public partial class RegistroSala : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ListarLocales();
        }

        public void registro() {
            try 
	        {	        
		     SalasServices.SalaServicesClient proxy = new SalasServices.SalaServicesClient();
            SalasServices.Sala salaNuevo = new SalasServices.Sala();
            SalasServices.Sala  salaCreado = null;

            //salaNuevo.Id_sala = 22;
            salaNuevo.Id_local = Convert.ToInt16(ddlLocal.SelectedValue);
            salaNuevo.Nombre = txtNombre.Text;
            salaNuevo.Capacidad = Convert.ToInt16(txtCapacidad.Text);
            salaNuevo.Descripcion = txtdescripcion.Text;            
            salaNuevo.Estado = ddlEstado.SelectedItem.Text;            
            salaCreado  = proxy.CrearSala(salaNuevo);
                if (salaCreado!= null)
                {
                string script = @"<script type='text/javascript'>alert('Se Registró Correctamente la sala " + salaCreado.Id_sala + "');</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }
                btnRegistro.Visible = false;
	        }
	        catch (Exception ex)
	        {

                lblMessage.Text = "ERROR: " + ex;
	        }
            
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            registro();
        }

        public void ListarLocales()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                "http://190.12.72.40/wslocales/LocalServices.svc/local");
            request.Method = "GET";
            HttpWebResponse response = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            List<Local> listaLocales = js.Deserialize<List<Local>>(tramaJson);
            ddlLocal.DataSource = listaLocales.ToList();
            //foreach (var item in listaLocales)
	        //{
                ddlLocal.DataValueField = "id_local";
                ddlLocal.DataTextField = "Nombre";
	        //}
            ddlLocal.DataBind();
            
        }


    }
}