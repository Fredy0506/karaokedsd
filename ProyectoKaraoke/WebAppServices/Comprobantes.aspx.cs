﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace WebAppServices
{
    public partial class Comprobantes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            TestBuscarComprobante();
        }

        public void TestBuscarComprobante()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                "http://localhost:2104/Comprobantes.svc/Comprobantes/F0001");
            request.Method = "GET";
            HttpWebResponse response = null;
            JavaScriptSerializer js = new JavaScriptSerializer();
            response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string tramaJson = reader.ReadToEnd();
            List<Comprobante> comprobanteObtener = js.Deserialize<List<Comprobante>>(tramaJson);
            dgComprobante.DataSource = comprobanteObtener.ToList();
            dgComprobante.DataBind();
        }

    }
}