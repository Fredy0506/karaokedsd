﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class Salas : System.Web.UI.Page
    {
        string cantHoras;
        string FechaInicio;
        string local;
        DateTime fecha;
        string horaReserva;

        int idlocal;
        int hora;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Reserva"] != null)
            {
                cantHoras = Session["CantidadHoras"].ToString();
                FechaInicio = Session["FechaInicio"].ToString();
                horaReserva= Session["HoraReserva"].ToString();
                local = Session["local"].ToString();
                fecha = Convert.ToDateTime(FechaInicio);

                idlocal = Convert.ToInt16(local);
                hora = Convert.ToInt16(cantHoras);
            }
            if (local != null)
            {
                divRegistroReserva.Visible = true;
                divRegistroSala.Visible = false;
                listaSalasDisp.Visible = true;
                listarSalasDisponibles(idlocal,hora,fecha);
            }
            else
            {
                divRegistroReserva.Visible = false;
                divRegistroSala.Visible = true;
                listaSalas.Visible = true;
                listarSalas();
            }
            
        }

        private void listarSalasDisponibles(int idlocal, int horas,DateTime fecha)
        {            
            try
            {
                SalasServices.SalaServicesClient proxy = new SalasServices.SalaServicesClient();
                List<SalasServices.Sala> lista = proxy.ListarSalasDisponibles(idlocal, fecha, horas).ToList<SalasServices.Sala>();

                divSalasDisp.Visible = true;
                dgSalasDisp.DataSource = lista;
                dgSalasDisp.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        private void listarSalas()
        {
            try
            {
                SalasServices.SalaServicesClient proxy = new SalasServices.SalaServicesClient();
                List<SalasServices.Sala> lista = proxy.ListarSalas().ToList<SalasServices.Sala>();

                dgSalas.DataSource = lista;
                dgSalas.DataBind();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "ERROR: " + ex;
            }

        }

        protected void dgSalas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void dgSalas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            dgSalas.PageIndex = e.NewPageIndex;
            listarSalas();
        }

        protected void dgSalasDisp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            string cantHoras = Session["CantidadHoras"].ToString();
            string FechaInicio = Session["FechaInicio"].ToString();
            string local = Session["local"].ToString();
            //string sala = Session["sala"].ToString(); ;
            DateTime fecha =Convert.ToDateTime(FechaInicio);

            dgSalasDisp.PageIndex = e.NewPageIndex;
            listarSalasDisponibles(Convert.ToInt16(local), Convert.ToInt16(cantHoras),fecha);
        }

        protected void dgSalasDisp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow gvrow = dgSalasDisp.Rows[index];
            //int codigo = Convert.ToInt16(gvrow.Cells[5].Text);
            Session["sala"] = gvrow.Cells[5].Text.Trim();

            if (e.CommandName.Equals("Modificar"))
            {
                //Session["CantidadHoras"] = ddlHorasReserva.SelectedValue;
                //Session["FechaInicio"] = txtHoraInicio.Value.Trim();
                //Session["local"] = ddlLocal.SelectedValue;
                //Session["sala"] = ddlSala.SelectedValue;
                Session["SalasDisponibles"] = "0";
                Response.Redirect("RegistrarReserva.aspx");
            }

            
        }
    }
}