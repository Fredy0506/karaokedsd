﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegistroLocal.aspx.cs" Inherits="WebAppServices.RegistroLocal" %>

<!DOCTYPE html />
<html lang="en">
<head>
		<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Sergey Pozhilov (GetTemplate.com)">
	
	<title>Registro de Local</title>

	<link rel="shortcut icon" href="images/gt_favicon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">

	<!-- Custom styles for our template -->
	<link rel="stylesheet" href="css/bootstrap-theme.css" media="screen" >
	<link rel="stylesheet" href="css/main.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.aspx"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li><a href="index.aspx">Inicio</a></li>
					<li><a href="about.aspx">Nosotros</a></li>
					<li><a href="contact.aspx">Contáctanos</a></li>
					<li class="active"><a class="btn" href="login.aspx">SIGN IN / SIGN UP</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->

	<header id="head" class="secondary"></header>

	<!-- container -->
	
	<!-- container -->
	<div class="container">   
		<div class="row">			
			<!-- Article main content -->
			<article class="col-xs-12 maincontent">
            								
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
					<div class="panel panel-default">
						<div class="panel-body">
							<h3 class="thin text-center">Registre un Nuevo Local</h3>							
							<hr>

							<form runat="server" id="form1">
								<div class="top-margin">
									<label>Nombre:</label>
                                <asp:TextBox ID="txtNombre" runat="server" class="form-control" ></asp:TextBox>
								</div>								
								<div class="top-margin">
									<label>Direccion:<span class="text-danger">*</span></label>
									<asp:TextBox ID="txtdireccion" runat="server" class="form-control" ></asp:TextBox>
								</div>
                                <div class="top-margin">
									<label>Telefono:<span class="text-danger">*</span></label>
									<asp:TextBox ID="txtTelefono" runat="server" class="form-control" ></asp:TextBox>
								</div>
                                <div class="top-margin">
									<label>Email:<span class="text-danger">*</span></label>
									<asp:TextBox ID="txtEmail" runat="server" class="form-control" ></asp:TextBox>
								</div>

                                <div class="top-margin">
									<label>Descripcion:<span class="text-danger">*</span></label>
									<asp:TextBox ID="txtDescripcion" runat="server" class="form-control" ></asp:TextBox>
								</div>

                                <div class="top-margin">
									<label>Estado:<span class="text-danger">*</span></label>
                                <asp:DropDownList ID="ddlEstado" runat="server">
                                    <asp:ListItem Value="01">Activo</asp:ListItem>
                                    <asp:ListItem Value="02">Inactivo</asp:ListItem>
                                    </asp:DropDownList>
								</div>

								
                                
								<hr>

								<div class="row">									
									<div class="col-lg-4 text-right">
                                <asp:Button ID="btnRegistro" runat="server" class="btn btn-action" Text="Registro" 
                                            onclick="btnRegistro_Click"></asp:Button>
                                            <asp:label id="lblMessage" runat="server" enableviewstate="false" />
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</article>
            <!-- /Article -->
            <div id="divlistado" runat="server"> 
            <center><b><a href="Locales.aspx">Listado de Locales</a></b></center> 
        </div>
	    </div>
     </div><!-- /container -->


	

	<footer id="footer" class="top-space">

		<div class="footer1">
			<div class="container">
				<div class="row">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p><br>
								
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Follow me</h3>
						<div class="widget-body">
							<p class="follow-me-icons clearfix">
								<a href=""><i class="fa fa-twitter fa-2"></i></a>
								<a href=""><i class="fa fa-dribbble fa-2"></i></a>
								<a href=""><i class="fa fa-github fa-2"></i></a>
								<a href=""><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">Sobre Nosotros</h3>
						<div class="widget-body">
							<a href="paginas/terms-of-use.html">Términos y condiciones</a><br>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2">
			<div class="container">
				<div class="row">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="#">Home</a> | 
								<a href="about.html">About</a> |
								<a href="sidebar-right.html">Sidebar</a> |
								<a href="contact.html">Contact</a> |
								<b><a href="signup.html">Sign up</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; 2014, Your name. Designed by <a href="http://gettemplate.com/" rel="designer">gettemplate</a> 
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>
	</footer>
</body>
</html>

