﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace WebAppServices
{
    public partial class RegistroLocal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            Local LocalAcrear = new Local()
            {
                Administrador="????",
                Descripcion = txtDescripcion.Text.Trim(),
                Direccion = txtdireccion.Text.Trim(),
                Estado = ddlEstado.SelectedItem.Text,
                Mail = txtEmail.Text.Trim(),
                Nombre = txtNombre.Text.Trim(),                
                Telefono = txtTelefono.Text.Trim()                
            };

            try
            {
                string postdata = js.Serialize(LocalAcrear);
                byte[] data = Encoding.UTF8.GetBytes(postdata);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://190.12.72.40/wslocales/LocalServices.svc/local");
                request.Method = "POST";
                request.ContentLength = data.Length;
                request.ContentType = "application/json";
                var requestStream = request.GetRequestStream();
                requestStream.Write(data, 0, data.Length);
                HttpWebResponse response = null;
                response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string tramaJson = reader.ReadToEnd();
                Local LocalCreado = js.Deserialize<Local>(tramaJson);

                string script = @"<script type='text/javascript'>alert('Se Registró Correctamente el Local con Codigo " + LocalCreado.id_local + "');</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                btnRegistro.Visible = false;
                divlistado.Visible = true;
            }
            catch (Exception ex)
            {

                lblMessage.Text = "ERROR: " + ex;
            }
            

            
        }
    }
}