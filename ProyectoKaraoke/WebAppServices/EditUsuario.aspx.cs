﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script;
namespace WebAppServices
{
    public partial class EditUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) { 
            string codigo=Request.QueryString["codigo"];
            hdnCodigo.Value = codigo;
            UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
            UsuarioService.Usuario usu = new UsuarioService.Usuario();
            usu=proxy.Obtener(codigo);
            txtNombre.Text = usu.nombre;
            txtApellido.Text = usu.apellido;
            txtEmail.Text = usu.correo;
            txtPassword.Text = usu.clave;
            imgFoto.ImageUrl  = usu.foto;
            }
            
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
                UsuarioService.Usuario usu = new UsuarioService.Usuario();
                usu.codigo = hdnCodigo.Value;
                usu.nombre = txtNombre.Text;
                usu.apellido = txtApellido.Text;
                usu.correo = txtEmail.Text;
                usu.clave = txtPassword.Text;
                usu.foto = imgFoto.ImageUrl;
                UsuarioService.Usuario usuario = null;
                usuario=proxy.ModificaUsuario(usu);
                if (usuario.codigo != null)
                {
                    string script = @"<script type='text/javascript'>alert('Se actualizo Correctamente');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }                
            }
            catch (Exception ex)
            {

                lblMessage.Text = "ERROR: " + ex;
            }
            


        }
    }
}