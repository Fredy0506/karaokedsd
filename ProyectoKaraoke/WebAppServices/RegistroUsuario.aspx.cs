﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppServices
{
    public partial class RegistroUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private string subirfoto()
        {
            string ruta="";
            if (FileUpload1.HasFile)
            {
                string nombreArchivo = FileUpload1.FileName;
                ruta = "~/img/" + nombreArchivo;
                FileUpload1.SaveAs(Server.MapPath(ruta));

            }
            return ruta;
        }

        protected void btnRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                UsuarioService.UsuariosClient proxy = new UsuarioService.UsuariosClient();
                UsuarioService.Usuario usu = new UsuarioService.Usuario();
                UsuarioService.Usuario usuario = null;
                usu.nombre=txtNombre.Text;
                usu.apellido=txtApellido.Text;
                usu.foto=subirfoto();
                usu.clave=txtPassword.Text;
                usu.correo = txtEmail.Text;
                usuario= proxy.CreaUsuario(usu);
                if (usuario.codigo != null)
                {
                    string script = @"<script type='text/javascript'>alert('Se Registró Correctamente el usuario con Codigo " + usuario.codigo +"');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "Popup", script, false);
                }

            }
            catch (Exception ex)
            {

                lblMessage.Text = "ERROR: " + ex;
            }
        }
    }
}