﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WSProducto.Dominio
{
    public class Producto
    {
        [DataMember]
        public int Codigo { get; set; }

        [DataMember]
        public string Nombre { get; set; }

        [DataMember]
        public decimal Precio { get; set; }

        [DataMember(IsRequired = false)]
        public string Disponibilidad { get; set; }

        [DataMember(IsRequired = false)]
        public int Cantidad { get; set; }
    }
}