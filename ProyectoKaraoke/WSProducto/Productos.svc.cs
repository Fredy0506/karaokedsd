﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WSProducto.Dominio;
using WSProducto.Errores;
using WSProducto.Persistencia;

namespace WSProducto
{
    public class Productos : IProductos
    {
        private ProductoADO ProductoDB = new ProductoADO();

        public Producto CrearProducto(Producto productoACrear)
        {
            if (ProductoDB.Obtener(productoACrear.Codigo) != null) //Ya existe
            {
                throw new FaultException<Excepciones>(
                    new Excepciones()
                    {
                        Codigo = "101",
                        Description = "El asesor ya existe",
                    },
                    new FaultReason("Error al intentar creación"));
            }
            return ProductoDB.Crear(productoACrear);
        }

        public void EliminarProducto(int codigo)
        {
            ProductoDB.Eliminar(codigo);
        }

        public Producto ModificarProducto(Producto productoAModificar)
        {
            return ProductoDB.Modificar(productoAModificar);
        }

        public Producto ObtenerProducto(int codigo)
        {
            return ProductoDB.Obtener(codigo);
        }

        public List<Producto> ListarProductos()
        {
            return ProductoDB.Listar();
        }
    }
}
