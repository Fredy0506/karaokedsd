﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WSProducto.Dominio;
using WSProducto.Persistencia;

namespace WSProducto
{
    [ServiceContract]
    public interface IProductos
    {

        [OperationContract]
        Producto CrearProducto(Producto productoACrear);

        [OperationContract]
        Producto ObtenerProducto (int codigo);

        [OperationContract]
        Producto ModificarProducto(Producto productoAModificar);

        [OperationContract]
        void  EliminarProducto(int codigo);

        [OperationContract]
        List<Producto> ListarProductos();

    }
}
