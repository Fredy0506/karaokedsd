﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WSProducto.Dominio;

namespace WSProducto.Persistencia
{
    public class ProductoADO
    {
        private string conectar = "Data Source=azrdb01.database.windows.net;" +
            "Initial Catalog=DSD;Persist Security Info=True;User ID=admazrdb01;Password=Pa$$w0rd";
        public Producto Crear(Producto productoACrear)
        {
            Producto nuevoProducto = null;
            string sql = "INSERT INTO t_productos VALUES (@cod, @nombre, @precio, @disponibilidad, @cantidad)";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", productoACrear.Codigo));
                    comando.Parameters.Add(new SqlParameter("@nombre", productoACrear.Nombre));
                    comando.Parameters.Add(new SqlParameter("@precio", productoACrear.Precio));
                    comando.Parameters.Add(new SqlParameter("@disponibilidad", productoACrear.Disponibilidad));
                    comando.Parameters.Add(new SqlParameter("@cantidad", productoACrear.Cantidad));
                    comando.ExecuteNonQuery();
                }
            }
            nuevoProducto = Obtener(productoACrear.Codigo);
            return productoACrear;
        }

        public Producto Obtener(int codigo) {
            Producto productoEncontrado = null;
            string sql = "SELECT * FROM t_productos WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            productoEncontrado = new Producto()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Nombre = (string)resultado["tx_nombre"],
                                Precio = (decimal)resultado["nu_precio"],
                                Disponibilidad = (string)resultado["tx_disponibilidad"],
                                Cantidad = (int)resultado["nu_cantidad"]
                            };
                        }
                    }
                }
            }
            return productoEncontrado;
        }

        public Producto Modificar(Producto productoAModificar) {
            Producto productoModificado = null;
            string sql = "UPDATE t_productos SET " +
                "tx_nombre=@nombre, nu_precio=@precio, tx_disponibilidad=@dis, nu_cantidad=@can WHERE nu_cod=@cod";
            using (SqlConnection conexion = new SqlConnection(conectar))
            {
                conexion.Open();
                using (SqlCommand comando = new SqlCommand(sql, conexion))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", productoAModificar.Codigo));
                    comando.Parameters.Add(new SqlParameter("@nombre", productoAModificar.Nombre));
                    comando.Parameters.Add(new SqlParameter("@precio", productoAModificar.Precio));
                    comando.Parameters.Add(new SqlParameter("@dis", productoAModificar.Disponibilidad));
                    comando.Parameters.Add(new SqlParameter("@can", productoAModificar.Cantidad));
                    comando.ExecuteNonQuery();
                }
            }
            productoModificado = Obtener(productoAModificar.Codigo);
            return productoModificado;
        }

        public void Eliminar(int codigo) {
            string sql = "DELETE FROM t_productos WHERE nu_cod=@cod";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sql, conex))
                {
                    comando.Parameters.Add(new SqlParameter("@cod", codigo));
                    comando.ExecuteNonQuery();
                }
            }
        }

        public List<Producto> Listar()
        {
            List<Producto> productosEncontrados = new List<Producto>();
            Producto productoEncontrado = null;
            string sql = "select * from t_productos";
            using (SqlConnection conex = new SqlConnection(conectar))
            {
                conex.Open();
                using (SqlCommand comando = new SqlCommand(sql, conex))
                {
                    using (SqlDataReader resultado = comando.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            productoEncontrado = new Producto()
                            {
                                Codigo = (int)resultado["nu_cod"],
                                Nombre = (string)resultado["tx_nombre"],
                                Precio = (decimal)resultado["nu_precio"],
                                Disponibilidad = (string)resultado["tx_disponibilidad"],
                                Cantidad = (int)resultado["nu_cantidad"]
                            };
                            productosEncontrados.Add(productoEncontrado);
                        }
                    }
                }
            }
            return productosEncontrados;
        }

    }
}