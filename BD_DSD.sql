USE [DSD]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 04/29/2017 15:58:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[id_user] [bigint] IDENTITY(1,1) NOT NULL,
	[codigo] [varchar](50) NULL,
	[nombre] [varchar](50) NULL,
	[apellido] [varchar](50) NULL,
	[contenido] [varchar](max) NULL,
	[foto] [varchar](50) NULL,
	[clave] [varchar](50) NULL,
	[correo] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT [dbo].[Usuario] ([id_user], [codigo], [nombre], [apellido], [contenido], [foto], [clave], [correo]) VALUES (1, N'U0001', N'Fredy', N'Fernandez', NULL, N'img/foto.jpg', N'123456', N'ffernandez.0506@gmail.com')
INSERT [dbo].[Usuario] ([id_user], [codigo], [nombre], [apellido], [contenido], [foto], [clave], [correo]) VALUES (2, N'U0002', N'Alfredo', N'Sabala', NULL, N'img/foto.jpg', N'123456', N'ffernandez.0506@gmail.com')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  Table [dbo].[Comprobante]    Script Date: 04/29/2017 15:58:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comprobante](
	[Codigo] [bigint] IDENTITY(1,1) NOT NULL,
	[Id_pedido] [bigint] NOT NULL,
	[NumeroFactura] [char](20) NOT NULL,
	[Direccion] [varchar](50) NOT NULL,
	[Telefono] [varchar](15) NOT NULL,
	[Fecha] [varchar](15) NOT NULL,
	[IGV] [numeric](18, 2) NOT NULL,
	[SubTotal] [numeric](18, 2) NOT NULL,
	[Total] [numeric](18, 2) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Comprobante] ON
INSERT [dbo].[Comprobante] ([Codigo], [Id_pedido], [NumeroFactura], [Direccion], [Telefono], [Fecha], [IGV], [SubTotal], [Total]) VALUES (1, 1, N'F0001               ', N'los jardines 1234', N'98437438', N'22-04-2017', CAST(18.00 AS Numeric(18, 2)), CAST(82.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)))
INSERT [dbo].[Comprobante] ([Codigo], [Id_pedido], [NumeroFactura], [Direccion], [Telefono], [Fecha], [IGV], [SubTotal], [Total]) VALUES (4, 2, N'F0002               ', N'los jardines 23242', N'98764522', N'22-5-2017', CAST(17.00 AS Numeric(18, 2)), CAST(83.00 AS Numeric(18, 2)), CAST(100.00 AS Numeric(18, 2)))
SET IDENTITY_INSERT [dbo].[Comprobante] OFF
